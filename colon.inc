; due to the structure of macro, we will have to define 
; dictionary in reversed order
; %1 - dictionary key
; %2 - mark name
%macro colon 2
%2:
    %ifdef next_value
        dq next_value
    %else
        dq 0
    %endif
    db %1, 0
    %define next_value, %2
%endmacro
