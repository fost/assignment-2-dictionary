ELF = -f elf64

main: main.o lib.o dict.o
	ld -o $@ $^

main.o: main.asm dict.o lib.o words.inc colon.inc lib.inc
	nasm $(ELF) -o $@ $<

dict.o: dict.asm lib.o lib.inc
	nasm $(ELF) -o $@ $<

lib.o: lib.asm
	nasm $(ELF) -o $@ $<

.PHONY: clean
clean:
	rm -f *.o
