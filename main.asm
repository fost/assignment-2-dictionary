%include "lib.inc"
%include "colon.inc"
%include "words.inc"
extern find_word

global _start

section .rodata
overflow_text: db `Length of string is longer than buffer\n`, 0
not_found_text: db `There is no such string in the dictionary as this`, 0

section .text
_start:
    sub rsp, 256

    mov rdi, rsp
    mov rsi, 256
    call read_sentence

    test rax, rax
    jz .io_error

    mov rdi, rsp
    mov rsi, next_value
    call find_word
    add rsp, 256

    test rax, rax
    jz .not_found

    mov rdi, rax
    call print_string
    call print_newline
    jmp exit
.io_error:
    add rsp, 256
    mov rdi, overflow_text
    call print_error
    mov rdi, 64
    jmp exit
.not_found:
    mov rdi, not_found_text
    call print_string
    call print_newline
    mov rdi, 0
    jmp exit