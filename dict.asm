%include "colon.inc"
%include "words.inc"
%include "lib.inc"

global find_word
; Params: 
;   rdi: null-term string (key)
;   rsi: dictionary start link
; Out:
;   rax: entry null-term string link with the given key, or 0 if failed
find_word:
.loop:
    push rsi
    push rdi
    add rsi, 8
    call string_equals

    test rax, rax
    jnz .found

    pop rdi
    pop rsi
    mov rsi, [rsi]

    test rsi, rsi
    jnz .loop

.not_found:
    xor rax, rax
    ret

.found:
    pop rdi
    call string_length

    pop rdi
    add rax, rdi
    add rax, 9 ; jump over next element link right to dict value
    ret